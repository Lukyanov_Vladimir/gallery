package com.example.gallery;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class FullScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        ImageView imageView = findViewById(R.id.FullScreenImage);
        TextView textView = findViewById(R.id.name);

        String path = getIntent().getStringExtra("path");
        String name = getIntent().getStringExtra("name");

        imageView.setImageDrawable(Drawable.createFromPath(path));
        textView.setText(name);
    }
}
